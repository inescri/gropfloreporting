/** Hippopotamus Controller **/
(function() {
	angular.module('zoop.groupFlo')
		.controller('DashboardHippoController', DashboardHippoController);

	DashboardHippoController.$inject = ['$scope', '$state', '$modal', '$timeout', '$interval', 'BrandService', 'GraphService'];

	function DashboardHippoController($scope, $state, $timeout, $modal, $interval, BrandService, GraphService) {
		var vm = this;
		vm.brand = BrandService;
		
		vm.shareViaEmail = {
			"title": "Partager par email"
		};

		/*NPS/OSAT*/
		vm.nps = {
			myscore: 55,
			pro: 70,
			pas: 15,
			det: 15,
			region: {pro: 50, pas: 30, det: 20},
			company: {pro: 65, pas: 15, det: 20},
			information: {
				"title": "Information",
				"content": "le NPS est l'indice de recommandation de vos clients"
			}
		};

		vm.satisfaction = {
			myscore: 90,
			region: 78,
			company: 69
		};

		vm.responseRate = {
			number: 230,
			percentage: 80
		};

		vm.alertes = {
			open: 2,
			pending: 10,
			closed: 30
		};

		vm.globalScore = {
			myscore: 80,
			region: 75,
			company: 77,
			categories: {
				esprit: 88,
				gout: 80,
				label: 89
			}
		};

		vm.focusMeat = {
			mysteryVisit: 89,
			survey: 37
		};

		// example verbatim comments
		vm.verbatim = {
			data: [{
				surveyID: 330,
				satLevel: 3,
				clientName: 'Elie Belair',
				surveyDate: '2016-11-04',
				shopName: 'TOURCOING',
				comment: 'A mon avis, vous avez le best steak du monde!',
				like: 4,
				timer: 3,
				clientLevel: 4
			},{
				surveyID: 334,
				satLevel: 2,
				clientName: 'Rei Solo',
				surveyDate: '2016-11-02',
				shopName: 'LILLE',
				comment: 'Eloko baeloko stolda tebya let mala malu. Kala Kala. Sivodna malay. Bayang magiliw perlas ng silanganan. Alab ng puso sa dibdib moy buhay. Hey you out there on the cold, always doing what you\'re told. Can you help me?',
				like: 2,
				timer: 2,
				clientLevel: 3
			}, {
				surveyID: 335,
				satLevel: 3,
				clientName: 'Cammille Cantabile',
				surveyDate: '2016-11-04',
				shopName: 'ROUBAIX',
				comment: 'Thelo malo smalo bahay maholi ioko helome aho. How can kolo roe retoke magakilo haligi nang kapatagan at kapayapaaan sa pandigma at depensa nang palakasan',
				like: 0,
				timer: 1,
				clientLevel: 1
			}, {
				surveyID: 339,
				satLevel: 2,
				clientName: 'Julien LeBrie',
				surveyDate: '2016-11-04',
				shopName: 'TOURCOING',
				comment: 'Koral foglo gloa, the long ieomer si alar bakar mask dela noya hunghang di manlang ksamila kolay ko mahal. Kalabaow ako haw haw de karabaw de batuta ni pulis wa went men. Uwa na you',
				like: 0,
				timer: 0,
				clientLevel: 4
			}, {
				surveyID: 340,
				satLevel: 332,
				clientName: 'Michel Michels',
				surveyDate: '2016-11-04',
				shopName: 'CALAIS',
				comment: 'Korobi akoklo kaneo biaek I morore na more ve. Ya dumayu, eto ne vashno. ne vashno ',
				like: 0,
				timer: -1,
				clientLevel: 2
			}, {
				surveyID: 343,
				satLevel: 332,
				clientName: 'Anais Belle',
				surveyDate: '2016-11-04',
				shopName: 'LILLE',
				comment: 'Korobi akoklo kaneo biaek I morore na more ve. Ya dumayu, eto ne vashno. ne vashno ',
				like: 0,
				timer: -2,
				clientLevel: 2
			}],
			shareDropdown: false,
			active: 0,
			next: function() {
				if ((vm.verbatim.active + 1) == vm.verbatim.data.length) {
					vm.verbatim.active = 0;
				} else {
					vm.verbatim.active++;
				}
			},
			setActive: function(i){
				vm.verbatim.active = i;
			},
			isActive: function(i) {
				return vm.verbatim.active == i;
			},
			like: function(i) {
				vm.verbatim.data[i].like++;
			},
			share: function(i) {
				vm.verbatim.shareDropdown = !vm.verbatim.shareDropdown;
			},
			rotate: function() {
				vm.verbatim.active 
				angular.forEach(vm.verbatim.data, function(v) {
					if (v.timer >=3) {
						v.timer = -2;
					} else {
						v.timer++;
					}
				});
			}
		}

		vm.topflop = {
			top: {
				data: [{
					label: 'Paris', score: 90
				}, {
					label: 'Rennes', score: 87
				}, {
					label: 'Strasbourg', score: 84
				}, {
					label: 'Roubaix', score: 81
				}, {
					label: 'Tourcoing', score: 80
				}]
			},
			flop: {
				data: [{
					label: 'Angers', score: 1
				}, {
					label: 'Le Havre', score: 3
				}, {
					label: 'Arromanches', score: 4
				}, {
					label: 'Nice', score: 15
				}, {
					label: 'Bordeaux', score: 35
				}]
			}
		}
		// Steps controls
		vm.hideStep = hideStep;
		vm.showStep = showStep;
		vm.toggleStep = toggleStep;
		vm.hideAllStep = hideAllStep;
		vm.showAllStep = showAllStep;

		vm.steps = [{
			hide: false,
			name: 'Environnement',
			data: [74, 84, 88],
			step: 1,
			icon: 'img/step-icon/environnement.png'
		}, {
			hide: false,
			name: 'Accueil',
			data: [45, 74, 88],
			step: 2,
			icon: 'img/step-icon/accueil.png'
		}, {
			hide: false,
			name: 'Prise de commande',
			data: [74, 85, 88],
			step: 3,
			icon: 'img/step-icon/prise de commande.png'
		}, {
			hide: false,
			name: 'D\351gustation du repas',
			data: [68, 78, 88],
			step: 4,
			icon: 'img/step-icon/degustation.png'
		}, {
			hide: false,
			name: 'Equipe',
			data: [88, 66, 75],
			step: 5,
			icon: 'img/step-icon/equipe.png'
		}, {
			hide: false,
			name: 'D\351part',
			data: [74, 70, 80],
			step: 6,
			icon: 'img/step-icon/depart.png'
		}];
		
		vm.showCounter = vm.steps.length;
		vm.hiddenSteps = [];
		hideAllStep();

		function hideStep(step){
			step.hide = true;
			vm.showCounter--;
			//$timeout(function() {vm.showCounter--;}, 500);
		}
		
		function showStep(step){
			vm.showCounter++;
			step.hide = false;
			//$timeout(function() {step.hide = false;vm.stepTempholder = false;}, 500);
		}
		
		function toggleStep(step) {
			if (step.hide) {
				showStep(step);
			} else {
				hideStep(step);
			}
		}
		
		function hideAllStep() {
			vm.showCounter = 0;
			angular.forEach(vm.steps, function(step) {
				step.hide=true;
			});
		}
		
		function showAllStep() {
			vm.showCounter = vm.steps.length ;
			angular.forEach(vm.steps, function(step) {
				step.hide=false;
			});
		}
		
		//cycling the the parole de client panel
		// $interval(function() {
		// 	vm.verbatim.rotate();
		// }, 6000);
		
		// Score global
		GraphService.drawPie("#scoreGlobal",{
			monScore: [80, 20],
			region: [70, 30],
			brand:[75,25]
		}, ['#cf002f', '#ddd']);

		// NPS Donut/Pie
		GraphService.drawPie("#npsPie",{
			nps: [70, 15, 15]
		}, ['#73a468', '#fbec70', '#ae484c']);

		// OSAT donut/pie
		GraphService.drawPie("#osatPie",{
			osat: [88, 12]
		}, //['#2fb332', '#fff200', '#fabe28', '#f13333']
			['#cf002f', '#eee']
		);
	}
		
}())
