(function() {
	'use strict';
	angular.module('zoop.groupFlo')
		.controller('DashboardTablaController', DashboardTablaController);
	
	DashboardTablaController.$inject = ['BrandService', 'GraphService'];
	
	function DashboardTablaController(BrandService, GraphService) {
		var vm = this;
		
		// Satisfaction Global
		GraphService.drawPie("#scoreGlobal",{
			myScore: [80, 20],
			region: [70, 30],
			brand: [75, 25]
		}, ['#f2800e', '#dfdfdf']);
	}
	
}());