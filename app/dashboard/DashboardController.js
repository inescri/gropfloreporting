(function() {
    var app = angular.module('zoop.groupFlo');
    
	app.controller('DashboardController', DashboardController);
	app.directive('zScoreGlo', zScoreGlo);

    DashboardController.$inject = ['$scope', '$state', 'BrandService'];

    function DashboardController($scope, $state, BrandService) {
        var vm = this;
        vm.brand = BrandService;
        // watching which brand dashboard to show
        $scope.$watch('vm.brand.active', function() {
            if(vm.brand.active === 'hippo') {
                $state.go('dashboard.hippo');
            } else {
                $state.go('dashboard.tabla');
            }
        });
    };
	
	function zScoreGlo() {
		return {
			templateUrl: 'app/dashboard/views/widget/score-global.html'
		};
	};
	
}())
