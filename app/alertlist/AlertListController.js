(function () {
	angular.module('zoop.groupFlo')
		.controller('AlertListController', AlertListController)
		.directive('zAlertsChart', zAlertsChart)
		.directive('zAlertsChartToggle', zAlertsChartToggle);

	function zAlertsChart() {
		var directive = {
			restrict: 'EA',
			template: '<div></div>',
			scope: {
				series: '='
			},
			link: function(scope, elem) {
				Highcharts.chart(elem[0], {
					title: {
						text: null
					},
					chart: {
						type: 'column',
						height: 260
					},
					credits: {
						enabled : false
					},
					xAxis: {
						categories: ['W1', 'W2', 'W3', 'W4', 'W5','W6','W7','W8', 'W9', 'W10']
					},
					yAxis: {
						title: {
							text: null
						}
					},
					tooltip: {
						shared: true
					},
					plotOptions: {
						series: {
							point: {
								events: {
									click: function() {
										scope.$emit('Z_ALERT_CHART_CLICKED', this.series)
									}
								}
							},
							stacking: 'normal'
						}
					},
					series: scope.series
				})
			}
		}
		return directive;
	}

	function zAlertsChartToggle() {
		var directive = {
			restrict: 'EA',
			template: '<i class="fa fa-window-maximize"></i> montrer/cacher',
			link: function(scope, elem) {
				elem.attr('role','button');
				elem.click(function() {
					$('.alert-chart-panel').slideToggle();
				})
			}
		};

		return directive;
	}

	AlertListController.$inject = ['$scope', '$filter', '$modal', 'uiGridGroupingConstants' ,'uiGridConstants'];

	function AlertListController($scope, $filter, $modal, uiGridGroupingConstants, uiGridConstants) {
		var vm = this;

		vm.responseRate = {
			number: 230,
			percentage: 80
		};

		vm.alertes = {
			open: 2,
			pending: 10,
			closed: 30
		};

		$scope.$on('Z_ALERT_CHART_CLICKED', function(event, data) {
			
			if (data.name == 'Trait\351') {
				vm.alertTableFilter = 3;
				$filter("filter")(vm.alertList,"Lille");
			}
			if (data.name == 'En cours') {
				vm.alertTableFilter = 2;
				$filter("filter")(vm.alertList,"Lille");
			}
			if (data.name == 'Non trait\351') {
				vm.alertTableFilter = 1;
				$filter("filter")(vm.alertList,"Lille");
			}

			vm.gridApi.core.notifyDataChange(uiGridConstants.dataChange.EDIT);
			
			})


		vm.surveyDetail = $modal({
			title: 'Enseigne',
			placement: 'center',
			backdrop: 'static',
			contentTemplate: 'app/modals/select-brand-content.html',
			keyboard: false,
			show: false,
			scope: $scope
		});

		vm.showSurveyDetail = function() {
			vm.surveyDetail.$promise.then(vm.surveyDetail.show);
		}

		vm.alertChartOptions = {
			series: [{
				data: [11, 15, 16, 14, 10, 12, 19, 18, 15, 11],
				name: 'Trait\351', color: '#73a468',
				code: 3
			}, {
				data: [0, 0, 0, 0, 0, 1, 0, 1, 2, 2],
				name: 'En cours', color: '#db892b',
				code: 2
			}, {
				data: [0, 0, 1, 0, 2, 0, 0, 0, 2, 7],
				name: 'Non trait\351', color: '#ae484c',
				code: 1
			}]
		}

		vm.alertList = [{
			status: 1,
			restaurant: 'Lille',
			client: 'Natalie Ciel',
			surveyDate: '2016-11-11',
			dateClosed: null,
			assignedTo: null,
			recommend: 2,
			statisfacton: 8
		}, {
			status: 2,
			restaurant: 'Nancy',
			client: 'Petronas Tour',
			surveyDate: '2016-11-10',
			dateClosed: null,
			assignedTo: 'Gustave Eiffel',
			recommend: 5,
			statisfacton: 4
		},{
			status: 1,
			restaurant: 'Strasbourg',
			client: 'Nina Yankovskaya',
			surveyDate: '2016-11-11',
			dateClosed: null,
			assignedTo: null,
			recommend: 2,
			statisfacton: 2
		}, {
			status: 2,
			restaurant: 'Strasbourg',
			client: 'Anakin Skywalker',
			surveyDate: '2016-11-11',
			dateClosed: null,
			assignedTo: 'Till Steiner',
			recommend: 2,
			statisfacton: 3
		}, {
			status: 3,
			restaurant: 'Lille',
			client: 'Martin Matin',
			surveyDate: '2016-11-09',
			dateClosed: '2016-11-10',
			assignedTo: 'Julie Jolie',
			recommend: 8,
			statisfacton: 9
		}, {
			status: 2,
			restaurant: 'Nancy',
			client: 'Joe Dassin',
			surveyDate: '2016-11-11',
			dateClosed: null,
			assignedTo: 'Gustave Eiffel',
			recommend: 2,
			statisfacton: 0
		}, {
			status: 2,
			restaurant: 'Lille',
			client: 'Roger De L\'eu',
			surveyDate: '2016-11-10',
			dateClosed: null,
			assignedTo: 'Michel Fuller',
			recommend: 3,
			statisfacton: 3
		}, {
			status: 2,
			restaurant: 'Roubaix',
			client: 'David Gilmour',
			surveyDate: '2016-11-10',
			dateClosed: null,
			assignedTo: 'Jame LaBrie',
			recommend: 1,
			statisfacton: 1
		}, {
			status: 3,
			restaurant: 'Lille',
			client: 'Anabelle Dopunt',
			surveyDate: '2016-11-09',
			dateClosed: '2016-11-10',
			assignedTo: 'Julie Jolie',
			recommend: 2,
			statisfacton: 3
		}, {
			status: 3,
			restaurant: 'Roubaix',
			client: 'Jose Manalao',
			surveyDate: '2016-11-08',
			dateClosed: '2016-11-11',
			assignedTo: 'Karen McAdams',
			recommend: 5,
			statisfacton: 5
		}, {
			status: 3,
			restaurant: 'Roubaix',
			client: 'Ron Luzon',
			surveyDate: '2016-11-08',
			dateClosed: '2016-11-11',
			assignedTo: 'Harry Mindanao',
			recommend: 8,
			statisfacton: 8
		}];

		//vm.alertList = vm.alertListAll;

		vm.gridOptions = {
			enableColumnMenus: false,
			columnDefs: [{
				name: ' ', 
				cellTemplate: '<div style="text-align:center"><a class="btn btn-xs btn-default consulter-button" role="button" href="#/survey-detail/1/alertant"><i class="fa fa-plus"></i></a></div>', 
				width: '3%'
			}, {
				name: 'Traitement alerte', 
				field: 'status',
				cellTemplate: '<div class="treatment-status status-{{row.entity.status}}">{{row.entity.status | treatmentStatusLabel}}</div>'
			}, {
				name: 'Restaurant', field: 'restaurant'
			}, {
				name: 'Nom du client', field: 'client'
			}, {
				name: 'Date du sondage', field: 'surveyDate'
			}, {
				name: 'Date de traitement', field: 'dateClosed'
			}, {
				name: 'Responsable du traitement', field: 'assignedTo'
			}, {
				name: 'RECO', 
				field: 'recommend', 
				width: '8%',
				cellTemplate: '<div ng-class="[\'reco-score\',\'score-\' + row.entity.recommend]">{{row.entity.recommend}}</div>'
			}, {
				name: 'SATISFACTION', 
				field: 'statisfacton', 
				width: '8%',
				cellTemplate: '<div ng-class="[\'sati-score\',\'score-\' + row.entity.statisfacton]">{{row.entity.statisfacton}}</div>'
			}],
			data: vm.alertList,
			onRegisterApi: function(gridApi) {
				vm.gridApi = gridApi;
			}
		};



	}
}())
