(function () {
	angular.module('zoop.groupFlo')
		.controller('ActionPlansListController', ActionPlansListController);
	
	ActionPlansListController.$inject = ['$modal', 'BrandService'];
	
	function ActionPlansListController($modal, BrandService) {
		var vm = this;
		
		vm.actionplans = [{
			name: 'Tit tak krasiva',
			dateCreation: '2016-10-12',
			dateDue: '2016-10-30',
			dateCompleted: null,
			status: 0,
			kpi: ['nps', 'csi']
		}, {
			name: 'Ya dumayu chto, eto interesno',
			dateCreation: '2016-10-12',
			dateDue: '2016-10-30',
			dateCompleted: '2016-10-12',
			status: 2,
			kpi: ['nps']
		}, {
			name: 'Kakaya bol, pyat nol',
			dateCreation: '2016-10-12',
			dateDue: '2016-10-30',
			dateCompleted: null,
			status: 1,
			kpi: ['csi']
		}, {
			name: 'kak u vas dela',
			dateCreation: '2016-10-12',
			dateDue: '2016-10-30',
			dateCompleted: null,
			status: 0,
			kpi: ['focus viande']
		}, {
			name: 'ty panimayesh, ines',
			dateCreation: '2016-10-12',
			dateDue: '2016-10-30',
			dateCompleted: null,
			status: 0,
			kpi: ['nps']
		}, {
			name: 'net net, ya ne znayu',
			dateCreation: '2016-10-12',
			dateDue: '2016-10-30',
			dateCompleted: null,
			status: 1,
			kpi: ['verbatim']
		}, {
			name: 'ya zhivu na filipinax',
			dateCreation: '2016-10-12',
			dateDue: '2016-10-30',
			dateCompleted: '2016-10-12',
			status: 2,
			kpi: ['focus viande']
		}];
		
	}
}())