(function() {
	angular.module('zoop.groupFlo')
		.controller('SurveyScoresController', SurveyScoresController)
		.directive('proDetEvolutionChart', proDetEvolutionChart)
		.directive('collapseEvolutionChart', collapseEvolutionChart);

	SurveyScoresController.$inject = ['uiGridGroupingConstants'];

	function SurveyScoresController(uiGridGroupingConstants) {
		var vm = this;

		vm.scores = [{
			restaurant: 'Lille', effectifs: 1, nps: 78, satisfaction:  81, focusViande: 88,
			q5: 78, q6: 71, q7: 82, q9: 76, q11: 82, q12: 85, q13: 86, q14: 80, q15: 77
		}, {
			restaurant: 'Strasbourg', effectifs: 1, nps: 90, satisfaction:  92, focusViande: 89,
			q5: 88, q6: 91, q7: 81, q9: 76, q11: 89, q12: 82, q13: 89, q14: 88, q15: 90
		}, {
			restaurant: 'Angers', effectifs: 1, nps: 43, satisfaction:  45, focusViande: 43,
			q5: 48, q6: 41, q7: 42, q9: 46, q11: 63, q12: 41, q13: 44, q14: 44, q15: 41
		}, {
			restaurant: 'Tourcoing', effectifs: 1, nps: -49, satisfaction:  25, focusViande: 33,
			q5: 48, q6: 32, q7: 30, q9: 36, q11: 64, q12: 70, q13: 29, q14: 34, q15: 21
		}, {
			restaurant: 'Paris', effectifs: 1, nps: 78, satisfaction:  81, focusViande: 88,
			q5: 73, q6: 72, q7: 84, q9: 75, q11: 83, q12: 89, q13: 82, q14: 83, q15: 90
		}, {
			restaurant: 'Nice', effectifs: 1, nps: 56, satisfaction:  66, focusViande: 57,
			q5: 55, q6: 58, q7: 59, q9: 53, q11: 55, q12: 57, q13: 56, q14: 53, q15: 52
		}, {
			restaurant: 'Nimes', effectifs: 1, nps: -10, satisfaction: 45, focusViande: 32,
			q5: 28, q6: 22, q7: 31, q9: 36, q11: 19, q12: 11, q13: 23, q14: 48, q15: 20
		}, {
			restaurant: 'Roubaix', effectifs: 1, nps: 100, satisfaction: 100, focusViande: 100,
			q5: 100, q6: 100, q7: 100, q9: 100, q11: 100, q12: 100, q13: 100, q14: 100, q51: 100
		}, {
			restaurant: 'Tours', effectifs: 1, nps: -49, satisfaction:  25, focusViande: 33,
			q5: 48, q6: 32, q7: 30, q9: 36, q11: 64, q12: 70, q13: 29, q14: 34, q15: 21
		}];
		
		vm.gridOptions = {
			enableSorting: true,
			enableColumnMenus: true,
			showColumnFooter: true,
			columnDefs: [{
				name: 'restaurant',
				width: '15%', pinnedLeft:true,
				footerCellTemplate: '<span>National</span>'
			}, {
				name: 'effectifs',
				width: '7%',
				footerCellTemplate: '<span>52</span>'
			}, {
				name: 'rapport_pdf',
				cellTemplate: '<div class="pdf-download"><i class="fa  fa-file-pdf-o"></i></div>'
			}, {
				name: 'nps',
				cellTemplate: '<div class="filled" ng-class="{\'score-good\': row.entity.nps >= 85, \'score-bad\': row.entity.nps < 75}">{{row.entity.nps}}</div>',
				footerCellTemplate: '<div class="score-score filled score-good" >85</div>'
			},{
				name: 'satisfaction',
				cellTemplate: '<div class="filled" ng-class="{\'score-good\': row.entity.satisfaction >= 85, \'score-bad\': row.entity.satisfaction < 75}">{{row.entity.satisfaction}}</div>',
				footerCellTemplate: '<div class="score-score filled score-good" >87</div>'
			},{
				name: 'focusViande',
				cellTemplate: '<div class="filled" ng-class="{\'score-good\': row.entity.focusViande >= 85, \'score-bad\': row.entity.focusViande < 75}">{{row.entity.focusViande}}</div>',
				footerCellTemplate: '<div class="score-score filled" >84</div>'
			},{
				name: 'q5',
				cellTemplate: '<div class="score-score" ng-class="{\'score-good\': row.entity.q5 >= 85, \'score-bad\': row.entity.q5 < 75}">{{row.entity.q5}}</div>',
				footerCellTemplate: '<div class="score-score  score-bad" >74</div>'
			},{
				name: 'q6',
				cellTemplate: '<div class="score-score" ng-class="{\'score-good\': row.entity.q6 >= 85, \'score-bad\': row.entity.q6 < 75}">{{row.entity.q6}}</div>',
				footerCellTemplate: '<div class="score-score  score-good" >85</div>'
			},{
				name: 'q7',
				cellTemplate: '<div class="score-score" ng-class="{\'score-good\': row.entity.q7 >= 85, \'score-bad\': row.entity.q7 < 75}">{{row.entity.q7}}</div>',
				footerCellTemplate: '<div class="score-score " >77</div>'
			},{
				name: 'q9',
				cellTemplate: '<div class="score-score" ng-class="{\'score-good\': row.entity.q9 >= 85, \'score-bad\': row.entity.q9 < 75}">{{row.entity.q9}}</div>',
				footerCellTemplate: '<div class="score-score score-good" >87</div>'
			},{
				name: 'q11',
				cellTemplate: '<div class="score-score" ng-class="{\'score-good\': row.entity.q11 >= 85, \'score-bad\': row.entity.q11 < 75}">{{row.entity.q11}}</div>',
				footerCellTemplate: '<div class="score-score score-bad" >73</div>'
			},{
				name: 'q12',
				cellTemplate: '<div class="score-score" ng-class="{\'score-good\': row.entity.q12 >= 85, \'score-bad\': row.entity.q12 < 75}">{{row.entity.q12}}</div>',
				footerCellTemplate: '<div class="score-score score-bad" >74</div>'
			},{
				name: 'q13',
				cellTemplate: '<div class="score-score" ng-class="{\'score-good\': row.entity.q13 >= 85, \'score-bad\': row.entity.q13 < 75}">{{row.entity.q13}}</div>',
				footerCellTemplate: '<div class="score-score score-good" >87</div>'
			},{
				name: 'q14',
				cellTemplate: '<div class="score-score" ng-class="{\'score-good\': row.entity.q14 >= 85, \'score-bad\': row.entity.q14 < 75}">{{row.entity.q14}}</div>',
				footerCellTemplate: '<div class="score-score " >77</div>'
			},{
				name: 'q15',
				cellTemplate: '<div class="score-score" ng-class="{\'score-good\': row.entity.q15 >= 85, \'score-bad\': row.entity.q15 < 75}">{{row.entity.q14}}</div>',
				footerCellTemplate: '<div class="score-score " >74</div>'
			}],
			data: vm.scores
		};

		var chart_categories = ['W1','W2','W3','W4','W5','W6','W7','W8','W9','W10', 'W11', 'W11', 'W12', 'W13'];

		vm.chartOptions = {
			chart: {
				type: 'area',
				height: 260
			},
			title: {
				text: null
			},
			xAxis: [{
				categories: chart_categories,
				reversed: false,
				labels: {
					step: 1
				}
			}],
			yAxis: {
				title: {
					text: null
				},
				labels: {
					formatter: function() {
						return Math.abs(this.value);
					}
				}
			},
			credits: {
				enabled: false
			},
			tooltip: {
				formatter: function() {
					var s = '';
					var nps = 0, i = 0;
					this.points.forEach(function(item) {
						if (i === 0) {
							nps = item.y;
						} else {
							nps = nps - item.y;
						}
						i++;
						s += '<br><span>' + item.series.name + '</span> : ' + item.y;
					})
					s = '<b>' + this.x + '</b><br> NPS : ' + nps + s;
					s = s + '<br><span>Effectifs</span> : 88' ;
					return s;
				},
				shared: true
			},
			series: [{
				name: 'Ambassadeurs',
				data: [67, 56, 67, 45, 33, 58, 51, 52, 54, 55, 56, 44, 54, 61],
				color: '#73a468',
			}, {
				name: 'D\351tracteurs',
				data: [17, 16, 27, 35, 44, 15, 16, 22, 14, 22, 15, 17, 22, 11],
				fillColor: 'rgba(255,255,255,0.75)',
				color: '#ae484c'
			}]
		};

	}

	function proDetEvolutionChart() {
		var directive = {
			restrict: 'EA',
			template: '<div></div>',
			scope: {
				options: '='
			},
			link: function(scope, elem) {
				Highcharts.chart(elem[0], scope.options)
			}
		}
		return directive;
	}

	function collapseEvolutionChart() {
		var directive = {
			restrict: 'EA',
			template: '<i class="fa fa-window-maximize"></i> montrer/cacher',
			link: function(scope, elem) {
				elem.attr('role','button');
				elem.click(function() {
					$('.pro-det-evolution-chart-container').slideToggle();
				})
			}
		};

		return directive;
	}

}())