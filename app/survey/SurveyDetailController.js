(function() {
	angular.module('zoop.groupFlo')
		.controller('SurveyDetailController', SurveyDetailController);

	SurveyDetailController.$inject = ['$stateParams', '$window'];

	function SurveyDetailController($stateParams, $window) {
		var vm = this;
		vm.stateParams = $stateParams;

		vm.survey = {
			questions: [{
				q: "Satisfaction derni\350re visite", a: 66, m: ["Lorem ipsum dolor"], verbatim: true
			}, {
				q: "Accueil convivial", a: 60
			}, {
				q: "Restaurant propre et bien tenu" , a: 51
			}, {
				q: "Ambiance agr\351able" , a: 81
			}, {
				q: "Plaisir \340 la d\351gustation", a: 52, m: ["Les accompagnements", "La viande de Bœuf"],  plus: true
			}, {
				q: "Recommandation pour la viande de boeuf", a: 67
			}, {
				q: "Sympathie, convivialit\351 de l\'\351quipe", a: 72
			}, {
				q: "Suggestion, conseils, explications sur le repas", a: "Non"
			}, {
				q: "Appr\351ciation des suggestions, conseils, explications", a: 88
			}, {
				q: "Equipe attentive, \340 l\'\351coute", a: 77
			}, {
				q: "Rythme du service", a: 77, m: ["Pour la prise de commande"], plus: true
			}, {
				q: "Tout a \351t\351 mis en oeuvre pour me satisfaire", a: 61
			}, {
				q: "Recommandation globale", a:50
			}, {
				q: "Remarque g\351n\351rale", a: "Non", verbatim: true
			}, {
				q: "Contexte", a: "XYZ"
			}, {
				q: "Accompagnant ", a: "Ipsum"
			}, {
				q: "Motif de venue ", a: "Lorem"
			}, {
				q: "Consommation" , a: "Lorem"
			}]
		};

		vm.getMoreInfoListPopover = function (list) {
			var tmpl = '';
			
			angular.forEach(list, function(item) {
				tmpl = tmpl  + item + ', ';
			});

			var popiver = '<div class="popover" tabindex="-1"><div class="arrow"></div><h3 class="popover-title" ng-bind="title" ng-show="title"></h3><div class="popover-content">CONCONCON</div></div>';
			return popiver.replace('CONCONCON', tmpl);
		}

		vm.goBack = function() {
			$window.history.back();
		}
	};

}())
