(function() {
    angular.module('zoop.groupFlo')
        .controller('SurveyListController', SurveyListController);

    function SurveyListController() {
        var vm = this;
        vm.surveys = [{
		clientStatus: 'alertant',
		restaurant: 'Lille',
		client: 'Natalie Ciel',
		surveyDate: '2016-11-11',
		transactDate: null,
		recommend: 20,
		statisfacton: 20,
		focus: 50
	}, {
		clientStatus: 'ambassadeur',
		restaurant: 'Nancy',
		client: 'Petronas Tour',
		surveyDate: '2016-11-10',
		transactDate: null,
		recommend: 94,
		statisfacton: 94,
		focus: 82
	},{
		clientStatus: null,
		restaurant: 'Strasbourg',
		client: 'Nina Yankovskaya',
		surveyDate: '2016-11-11',
		transactDate: null,
		recommend: 84,
		statisfacton: 85,
		focus: 55
	}, {
		clientStatus: null,
		restaurant: 'Strasbourg',
		client: 'Anakin Skywalker',
		surveyDate: '2016-11-11',
		transactDate: '2016-11-01',
		recommend: 76,
		statisfacton: 87,
		focus: 69
	}, {
		clientStatus: null,
		restaurant: 'Lille',
		client: 'Martin Matin',
		surveyDate: '2016-11-09',
		transactDate: '2016-11-10',
		recommend: 80,
		statisfacton: 70,
		focus: 74
	}, {
		clientStatus: 'ambassadeur',
		restaurant: 'Nancy',
		client: 'Joe Dassin',
		surveyDate: '2016-11-11',
		transactDate: null,
		recommend: 90,
		statisfacton: 10,
		focus: 53
	}, {
		clientStatus: null,
		restaurant: 'Lille',
		client: 'Roger De L\'eu',
		surveyDate: '2016-11-10',
		transactDate: null,
		recommend: 70,
		statisfacton: 75,
		focus: 75
	}, {
		clientStatus: 'alertant',
		restaurant: 'Roubaix',
		client: 'David Gilmour',
		surveyDate: '2016-11-10',
		transactDate: null,
		recommend: 10,
		statisfacton: 36,
		focus: 19
	}, {
		clientStatus: null,
		restaurant: 'Lille',
		client: 'Anabelle Dopunt',
		surveyDate: '2016-11-09',
		transactDate: '2016-11-10',
		recommend: 71,
		statisfacton: 73,
		focus: 0
	}, {
		clientStatus: 'ambassadeur',
		restaurant: 'Roubaix',
		client: 'Jose Manalao',
		surveyDate: '2016-11-08',
		transactDate: '2016-11-11',
		recommend: 10,
		statisfacton: 10,
		focus: 0
	}, {
		clientStatus: null,
		restaurant: 'Roubaix',
		client: 'Ron Luzon',
		surveyDate: '2016-11-08',
		transactDate: '2016-11-11',
		recommend: 85,
		statisfacton: 75,
		focus: 20
	}];

	//vm.alertList = vm.alertListAll;

	vm.gridOptions = {
		enableColumnMenus: false,
		columnDefs: [{
			name: ' ', 
			cellTemplate: '<div style="text-align:center"><a class="btn btn-xs btn-default consulter-button" role="button" href="#/survey-detail/1/{{row.entity.clientStatus}}"><i class="fa fa-plus"></i></a></div>', 
			width: '3%'
		}, {
			name: 'Alertant / Ambassadeur', 
			field: 'clientStatus',
			headerTooltip: 'Indicateur bas\351 sur la question de recommandation',
			cellTemplate: '<div class="client-status"><div ng-class="[\'client-status-icon\', row.entity.clientStatus]" >'+
				'<span ng-class="{\'fa fa-star\':row.entity.clientStatus == \'ambassadeur\',\'entypo-attention\':row.entity.clientStatus == \'alertant\'}"></span>'+
				'</div></div>'
		}, {
			name: 'Restaurant', field: 'restaurant'
		}, {
			name: 'Nom du client', field: 'client'
		}, {
			name: 'Date du sondage', field: 'surveyDate'
		}, {
			name: 'Date de passage', field: 'transactDate'
		}, {
			name: 'RECO', 
			field: 'recommend', 
			width: '8%',
			cellTemplate: '<div class="filled" ng-class="{\'score-good\': row.entity.recommend >= 85, \'score-bad\': row.entity.recommend < 75}">{{row.entity.recommend}}</div>'
		}, {
			name: 'SATISFACTION', 
			field: 'statisfacton', 
			width: '8%',
			cellTemplate: '<div class="filled" ng-class="{\'score-good\': row.entity.statisfacton >= 85, \'score-bad\': row.entity.statisfacton < 75}">{{row.entity.statisfacton}}</div>'

		},{
			name: 'Focus Viande', 
			field: 'focusViande',
			width: '8%',
			cellTemplate: '<div class="filled" ng-class="{\'score-good\': row.entity.focus >= 85, \'score-bad\': row.entity.focus < 75}">{{row.entity.focus}}</div>'
		}],
		data: vm.surveys
	};
    }



}())