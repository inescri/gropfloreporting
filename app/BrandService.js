(function() {
	angular.module('zoop.groupFlo')
		.service('BrandService', BrandService);

	//BrandService.$inject = ['$scope'];
	function BrandService() {
		var service = this;
		service.active = 'hippo';
		service.setActive = setActive;
		service.getTitle = getTitle;
		service.isActive = isActive;

		function setActive(brand) {
			service.active = brand;
			//$scope.$broadcast('NEW_BRAND_SET', brand);
		}

		function getTitle() {
			if (service.active === 'hippo') {
				return 'Hippopotamus';
			}
			return 'Tabla Pizza';
		}

		function isActive(brand) {
			return service.active === brand;
		}
	}
}())
