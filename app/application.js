(function() {
	angular.module('zoop.groupFlo', [
		'ui.router',
		'ui.grid',
		'ui.date',
		'ui.grid.pinning','ui.grid.grouping',
		'tc.chartjs',
		'mgcrea.ngStrap',
		'ngAnimate'
	])

	.controller('PageTitleController', function($scope, $state) {
		var vm = this;
		vm.pageTitle = $state;
	})

	.filter('treatmentStatusLabel', function() {
		return function (input) {
			var map =  [null,'non trait\351', 'en cours','trait\351'];
			if (map[input]) {
				return map[input];
			} else {
				return input;
			}
		}
	})

}())