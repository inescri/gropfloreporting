(function() {
	angular.module('zoop.groupFlo')
		.controller('MysteryVisitsListController', MysteryVisitsListController)
		.directive('vmServiceIcon', vmServiceIcon);
	
	function vmServiceIcon() {
		return {
			template: '<i></i>',
			restrict: 'AE',
			scope: {
				vmServiceIcon: '@'
			},
			link: function(scope, elem, attr) {
				var i = scope.vmServiceIcon;
				i = i.split(' ');
				var icon = i[1], lbl = i[0];
				if (icon == 'midi') {
					elem.find('i').addClass('wi wi-fw wi-day-sunny')
				} 
				if (icon == 'soir') {
					elem.find('i').addClass('wi wi-night-clear wi-fw')
				}
				if (icon == 'matin') {
					elem.find('i').addClass('wi wi-sunrise wi-fw')
				}
				elem.find('i').attr('title', icon);
				elem.append(lbl)
			}
		}
	};

	MysteryVisitsListController.$inject = ['uiGridGroupingConstants', 'uiGridConstants'];
	function MysteryVisitsListController(uiGridGroupingConstants, uiGridConstants) {
		var vm = this;

		vm.surveys = [{
			idVisite: 1301,
			restaurant: 'Tourcoing',
			period: 'Janvier 2017',
			service: 'week-end midi',
			scoreGlobal: 80,
			cat_esprit: 80,
			cat_gout: 80,
			cat_label: 75,
			stg_environment:  80 ,
			stg_accueil: 80,
			stg_prise_de_commande: 80,
			stg_equipe: 80,
			stg_depart: 80
		},{
			idVisite: 2342,
			restaurant: 'Lille',
			period: 'Janvier 2017',
			service: 'Semaine midi',
			scoreGlobal: 97,
			cat_esprit: 97,
			cat_gout: 98,
			cat_label: 97,
			stg_environment:  97 ,
			stg_accueil: 97,
			stg_prise_de_commande: 97,
			stg_equipe: 97,
			stg_depart: 97

		}, {
			idVisite: 1334,
			restaurant: 'Strasbourg',
			period: 'Janvier 2017',
			service: 'week-end matin',
			scoreGlobal: 71,
			cat_esprit: 71,
			cat_gout: 71,
			cat_label: 74,
			stg_environment:  71 ,
			stg_accueil: 71,
			stg_prise_de_commande: 71,
			stg_equipe: 71,
			stg_depart: 71
		}, {
			idVisite: 2332,
			restaurant: 'Roubaix',
			period: 'Janvier 2017',
			service: 'Semaine soir',
			scoreGlobal: 100,
			cat_esprit: 100,
			cat_gout: 100,
			cat_label: 100,
			stg_environment:  98 ,
			stg_accueil: 100,
			stg_prise_de_commande: 100,
			stg_equipe: 100,
			stg_depart: 100
		}, {
			idVisite: 2337,
			restaurant: 'Roubaix',
			period: 'Janvier 2017',
			service: 'week-end midi',
			scoreGlobal: 80,
			cat_esprit: 80,
			cat_gout: 80,
			cat_label: 75,
			stg_environment:  80 ,
			stg_accueil: 80,
			stg_prise_de_commande: 80,
			stg_equipe: 80,
			stg_depart: 80
		}, {
			idVisite: 5513,
			restaurant: 'Strasbourg',
			period: 'Janvier 2017',
			service: 'semaine soir',
			scoreGlobal: 85,
			cat_esprit: 88,
			cat_gout: 71,
			cat_label: 74,
			stg_environment:  97 ,
			stg_accueil: 71,
			stg_prise_de_commande: 71,
			stg_equipe: 71,
			stg_depart: 81
		}, {
			idVisite: 2332,
			restaurant: 'Roubaix',
			period: 'Janvier 2017',
			service: 'Semaine soir',
			scoreGlobal: 100,
			cat_esprit: 100,
			cat_gout: 100,
			cat_label: 100,
			stg_environment:  98 ,
			stg_accueil: 100,
			stg_prise_de_commande: 100,
			stg_equipe: 100,
			stg_depart: 100
		}];

		vm.gridOptions = {
			enableSorting: true,
			enableColumnMenus: true,
			columnDefs: [{
				name: 'id visite', field: 'idVisite',
				width: 100
			}, {
				name: 'Restaurant', field: 'restaurant', width: 140
			}, {
				name: 'P\351riode', field: 'period',
				width: 110
			}, {
				name: 'Service', field: 'service',
				cellTemplate: '<div class="service-time"><span vm-service-icon="{{row.entity.service}}"></span></div>',
				width: 120
			}, {
				name: 'rapport pdf', 
				cellTemplate: '<div class="pdf-download"><i class="fa  fa-file-pdf-o"></i></div>',
				width: 100
			}, {
				name: 'Score Global', field: 'scoreGlobal',
				cellTemplate: '<div ng-class="{\'score-good\' : row.entity.scoreGlobal > 90, \'score-bad\' : row.entity.scoreGlobal <= 75 }" class="score-score filled">{{row.entity.scoreGlobal}}</div>',
				width: 90
			}, {
				name: 'Esprit Hippo', field: 'cat_esprit',
				cellTemplate: '<div ng-class="{\'score-good\' : row.entity.cat_esprit > 90, \'score-bad\' : row.entity.cat_esprit <= 75 }" class="score-score">{{row.entity.cat_esprit}}</div>',
				width: 90
			}, {
				name: 'Gout Hippo', field: 'cat_gout',
				cellTemplate: '<div ng-class="{\'score-good\' : row.entity.cat_gout > 90, \'score-bad\' : row.entity.cat_gout <= 75 }" class="score-score">{{row.entity.cat_gout}}</div>',
				width: 90
			}, {
				name: 'Label Hippo', field: 'cat_label',
				cellTemplate: '<div ng-class="{\'score-good\' : row.entity.cat_label > 90, \'score-bad\' : row.entity.cat_label <= 75 }" class="score-score">{{row.entity.cat_label}}</div>',
				width: 90
			}, {
				name: 'Environment', field: 'stg_environment',
				cellTemplate: '<div ng-class="{\'score-good\' : row.entity.stg_environment > 90, \'score-bad\' : row.entity.stg_environment <= 75 }" class="score-score">{{row.entity.stg_environment}}</div>',
				width: 90
			}, {
				name: 'Accueil', field: 'stg_accueil',
				cellTemplate: '<div ng-class="{\'score-good\' : row.entity.stg_accueil > 90, \'score-bad\' : row.entity.stg_accueil <= 75 }" class="score-score">{{row.entity.stg_accueil}}</div>',
				width: 90
			}, {
				name: 'Prise de commande', field: 'stg_prise_de_commande',
				cellTemplate: '<div ng-class="{\'score-good\' : row.entity.stg_prise_de_commande > 90, \'score-bad\' : row.entity.stg_prise_de_commande <= 75 }" class="score-score">{{row.entity.stg_prise_de_commande}}</div>',
				width: 90
			}, {
				name: 'Equipe', field: 'stg_equipe',
				cellTemplate: '<div ng-class="{\'score-good\' : row.entity.stg_equipe > 90, \'score-bad\' : row.entity.stg_equipe <= 75 }" class="score-score">{{row.entity.stg_equipe}}</div>',
				width: 90
			}, {
				name: 'Depart', field: 'stg_depart',
				cellTemplate: '<div ng-class="{\'score-good\' : row.entity.stg_depart > 90, \'score-bad\' : row.entity.stg_depart <= 75 }" class="score-score">{{row.entity.stg_depart}}</div>',
				width: 80
			}],
			data: vm.surveys
		}

		vm.getCssClass = function(score) {
			return score;
		}


	}

}())
