(function() {
	angular.module('zoop.groupFlo')
		.controller('MysteryVisitsScoresController', MysteryVisitsScoresController)
		.directive('zShowHideCols', zShowHideCols);
	
	function zShowHideCols() {
		var directive = {
			template: '',
			scope: {
				dropdown: '@'
			},
			restrict: 'AE',
			link: function(scope, elem) {
				elem.on('click', function() {
					$(scope.dropdown).slideToggle();
				});
			}
		};
		
		return directive;
	}
	
	MysteryVisitsScoresController.$inject = ['uiGridGroupingConstants', 'uiGridConstants'];
	function MysteryVisitsScoresController(uiGridGroupingConstants, uiGridConstants) {
		var vm = this;
		
		vm.scores = [{
			shop: "Lille",
			region: "Hauts-de-France",
			globalScore: 77,
			visits: 3,
			cat_esprit: 77,
			cat_gout: 82,
			cat_label: 89,
			stg_environment: 55, 
			stg_accueil: 77,
			stg_prise_de_commande: 83,
			stg_equipe: 88,
			stg_depart: 67
		}, {
			shop: "Roubaix",
			region: "Hauts-de-France",
			globalScore: 78,
			visits: 4,
			cat_esprit: 73,
			cat_gout: 82,
			cat_label: 90,
			stg_environment: 78, 
			stg_accueil: 78,
			stg_prise_de_commande: 76,
			stg_equipe: 78,
			stg_depart: 77
		} , {
			shop: "Tourcoing",
			region: "Hauts-de-France",
			globalScore: 67,
			visits: 3,
			cat_esprit: 77,
			cat_gout: 61,
			cat_label: 78,
			stg_environment: 88, 
			stg_accueil: 80,
			stg_prise_de_commande: 76,
			stg_equipe: 68,
			stg_depart: 89
		}, {
			shop: "Paris",
			region: "Ile-de-France",
			globalScore: 81,
			visits: 5,
			cat_esprit: 72,
			cat_gout: 89,
			cat_label: 89,
			stg_environment: 80, 
			stg_accueil: 89,
			stg_prise_de_commande: 81,
			stg_equipe: 88,
			stg_depart: 72
		}, {
			shop: "Strasbourg",
			region: "Grand Est",
			globalScore: 89,
			visits: 2,
			cat_esprit: 72,
			cat_gout: 69,
			cat_label: 89,
			stg_environment: 72, 
			stg_accueil: 60,
			stg_prise_de_commande: 71,
			stg_equipe: 88,
			stg_depart: 67
		}, {
			shop: "Lorraine",
			region: "Grand Est",
			globalScore: 71,
			visits: 2,
			cat_esprit: 67,
			cat_gout: 88,
			cat_label: 89,
			stg_environment: 78, 
			stg_accueil: 87,
			stg_prise_de_commande: 77,
			stg_equipe: 78,
			stg_depart: 77
		}, {
			shop: "Rennes",
			region: "Bretagne",
			globalScore: 79,
			visits: 2,
			cat_esprit: 78,
			cat_gout: 84,
			cat_label: 79,
			stg_environment: 81, 
			stg_accueil: 76,
			stg_prise_de_commande: 82,
			stg_equipe: 76,
			stg_depart: 83
		}, {
			shop: "Toulouse",
			region: "Occitanie",
			globalScore: 87,
			visits: 2,
			cat_esprit: 88,
			cat_gout: 71,
			cat_label: 89,
			stg_environment: 78, 
			stg_accueil: 90,
			stg_prise_de_commande: 78,
			stg_equipe: 72,
			stg_depart: 73
		}, {
			shop: "Charente",
			region: "Nouvelle-Aquitaine",
			globalScore: 77,
			visits: 4,
			cat_esprit: 78,
			cat_gout: 76,
			cat_label: 78,
			stg_environment: 74, 
			stg_accueil: 75,
			stg_prise_de_commande: 75,
			stg_equipe: 72,
			stg_depart: 77
		}, {
			shop: "Bordeaux",
			region: "Nouvelle-Aquitaine",
			globalScore: 72,
			visits: 4,
			cat_esprit: 72,
			cat_gout: 72,
			cat_label: 49,
			stg_environment: 73, 
			stg_accueil: 77,
			stg_prise_de_commande: 76,
			stg_equipe: 74,
			stg_depart: 73
		}, {
			shop: "Angers",
			region: "Nouvelle-Aquitaine",
			globalScore: 71,
			visits: 4,
			cat_esprit: 55,
			cat_gout: 57,
			cat_label: 49,
			stg_environment: 57, 
			stg_accueil: 73,
			stg_prise_de_commande: 76,
			stg_equipe: 75,
			stg_depart: 56
		}];
		
		vm.gridOptions = {
			enableSorting: true,
			enableColumnMenus: true,
			showColumnFooter: true,
			columnDefs: [
				{ name: "Restaurant", field: "shop", pinnedLeft:true, width: 150, footerCellTemplate: '<span>National</span>' },
				{ name: "Score Global", field: "globalScore", width: 130,
					cellTemplate: '<div class="score-score filled" ng-class="{\'score-good\': row.entity.globalScore >= 85, \'score-bad\': row.entity.globalScore < 75}">{{row.entity.globalScore}}</div>',
				 	footerCellTemplate: '<div class="score-score filled score-good" >85</div>'
				},
				{ name: "Esprit Hippo", field: "cat_esprit",
					cellTemplate: '<div class="score-score" ng-class="{\'score-good\': row.entity.cat_esprit >= 85, \'score-bad\': row.entity.cat_esprit < 75}">{{row.entity.cat_esprit}}</div>',
					footerCellTemplate: '<div class="score-score  score-bad" >74</div>'
				},
				{ name: "Gout Hippo", field: "cat_gout",
					cellTemplate: '<div class="score-score" ng-class="{\'score-good\': row.entity.cat_gout >= 85, \'score-bad\': row.entity.cat_gout < 75}">{{row.entity.cat_gout}}</div>',
					footerCellTemplate: '<div class="score-score " >78</div>'
				},
				{ name: "Label Hippo", field: "cat_label",
					cellTemplate: '<div class="score-score" ng-class="{\'score-good\': row.entity.cat_label >= 85, \'score-bad\': row.entity.cat_label < 75}">{{row.entity.cat_label}}</div>',
					footerCellTemplate: '<div class="score-score  score-good" >86</div>'
				},
				{ name: "Environment", field: "stg_environment",
					cellTemplate: '<div class="score-score" ng-class="{\'score-good\': row.entity.stg_environment >= 85, \'score-bad\': row.entity.stg_environment < 75}">{{row.entity.stg_environment}}</div>',
					footerCellTemplate: '<div class="score-score score-bad" >73</div>'
				},
				{ name: "Accueil", field: "stg_accueil",
					cellTemplate: '<div class="score-score" ng-class="{\'score-good\': row.entity.stg_accueil >= 85, \'score-bad\': row.entity.stg_accueil < 75}">{{row.entity.stg_accueil}}</div>',
					footerCellTemplate: '<div class="score-score" >81</div>'
				 },
				{ name: "Q1", field: "stg_accueil_q1", visible: false,
					cellTemplate: '<div class="score-score" ng-class="{\'score-good\': row.entity.stg_accueil >= 85, \'score-bad\': row.entity.stg_accueil < 75}">{{row.entity.stg_accueil}}</div>',
					footerCellTemplate: '<div class="score-score" >80</div>'
				 },
				{ name: "Prise de commande", field: "stg_prise_de_commande",
					cellTemplate: '<div class="score-score" ng-class="{\'score-good\': row.entity.stg_prise_de_commande >= 85, \'score-bad\': row.entity.stg_prise_de_commande < 75}">{{row.entity.stg_prise_de_commande}}</div>',
					footerCellTemplate: '<div class="score-score score-bad" >75</div>'
				},{ name: "Equipe", field: "stg_equipe",
					cellTemplate: '<div class="score-score" ng-class="{\'score-good\': row.entity.stg_equipe >= 85, \'score-bad\': row.entity.stg_equipe < 75}">{{row.entity.stg_equipe}}</div>',
					footerCellTemplate: '<div class="score-score score-bad" >75</div>'
				},{ name: "Depart", field: "stg_depart", 
					cellTemplate: '<div class="score-score" ng-class="{\'score-good\': row.entity.stg_depart >= 85, \'score-bad\': row.entity.stg_depart < 75}">{{row.entity.stg_depart}}</div>',
					footerCellTemplate: '<div class="score-score score-good" >87</div>'
				}
			],
			onRegisterApi: function(gridApi) {
				vm.gridApi = gridApi;
			},
			data: vm.scores
		};
		
		// demonstration to show hide columns in the table
		vm.hideColumn = function(index) {
			vm.gridOptions.columnDefs[index].visible = false;
			vm.gridApi.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);
		};
		
		vm.showColumn = function(index) {
			vm.gridOptions.columnDefs[index].visible = true;
			vm.gridApi.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);
		};
		
		vm.refreshCols = function() {
			vm.gridApi.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);
		}
		vm.showQ1 = function() {
			vm.showColumn(7);
		};
	
	}

}())