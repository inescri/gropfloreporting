(function () {
	angular.module('zoop.groupFlo')
		.service('GraphService', GraphService);
	
	function GraphService() {
		var me = this;
		// drawing multi ring pie using d3
		me.drawPie = function (elem, dataset, color) {
			
			var width = 200,
				height = 200,
				cwidth = 15;

			var pie = d3.layout.pie()
				.sort(null);

			var arc = d3.svg.arc();

			var div = d3.select("body").append("div")   
				.attr("class", "zz-tooltip")               
				.style("opacity", 1);

			var svg = d3.select(elem).append("svg")
				.attr("width", width)
				.attr("height", height)
				.append("g")
				.attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

			var gs = svg.selectAll("g").data(d3.values(dataset)).enter().append("g");
			var path = gs.selectAll("path")
				.data(function(d) { return pie(d); })
				.enter().append("path")
				.attr("fill", function(d, i) { return color[i]; })
				.attr("class", function(d, i, j) { console.log("pie-ring-" + j + "-" + i); return "pie-ring-" + j + " pie-ring-fill-" + i})
				.attr("d", function(d, i, j) { return arc.innerRadius(5+cwidth*(j+3)).outerRadius(cwidth*(j+4))(d); })
				.on("mouseover", function(d, i, j) {
					div.transition()        
					.duration(200)      
					.style("opacity", .9);      
					div.html(d.value)  
					.style("left", (d3.event.pageX) + "px")     
					.style("top", (d3.event.pageY - 28) + "px");
				})
				.on("mouseout", function(d) {
					div.transition()        
					.duration(500)      
					.style("opacity", 0);   
				});
		};
		
	}
}())