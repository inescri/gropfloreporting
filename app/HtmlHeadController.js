(function() {
	angular.module('zoop.groupFlo')
		.controller('HtmlHeadController', HtmlHeadController);

	HtmlHeadController.$inject = ['$scope', '$modal', 'BrandService'];

	function HtmlHeadController($scope, $modal, BrandService) {
		var h = this;
		h.brand = BrandService;
		h.selectBrandModal = $modal({
			title: 'Enseigne',
			placement: 'center',
			backdrop: 'static',
			contentTemplate: 'app/modals/select-brand-content.html',
			keyboard: false,
			show: false,
			scope: $scope
		});
	}
}())
