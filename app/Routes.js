/** UI router configuration **/
(function() {
	angular.module('zoop.groupFlo')
		.config(function($stateProvider, $urlRouterProvider) {

			$urlRouterProvider.otherwise('/dashboard');

			$stateProvider
			.state('login', {
				url: '/login',
				templateUrl: 'app/login/login.html'
			})
			.state('dashboard', {
				url: '/dashboard',
				templateUrl: 'app/dashboard/views/dashboard.html',
				controller: 'DashboardController as vm',
			})
			.state('dashboard.hippo', {
				url: '/hippo',
				templateUrl: 'app/dashboard/views/dashboard-hippo.html',
				controller: 'DashboardHippoController as vm',
				title: 'Tableau de bord / Hippo / \307a c\'est Hippo !'
			})
			.state('dashboard.tabla', {
				url: '/tabla',
				templateUrl: 'app/dashboard/views/dashboard-tabla.html',
				controller: 'DashboardTablaController as vm',
				title: 'Tableau de bord / Tabla'
			})
			.state('mystery-visits', {
				url: '/mystery-visits',
				templateUrl: 'app/mystery-visits/views/mystery-visits.html',
				controller: 'MysteryVisitsController as vm',
			})
			.state('mystery-visits.scores', {
				url: '/scores',
				templateUrl: 'app/mystery-visits/views/scores.html',
				controller: 'MysteryVisitsScoresController as vm',
				title: 'Visite Mystere / Scores'
			})
			.state('mystery-visits.list', {
				url: '/list',
				templateUrl: 'app/mystery-visits/views/list.html',
				controller: 'MysteryVisitsListController as vm',
				title: 'Visite Mystere / Liste'
			})
			.state('alertlist', {
				url: '/alertlist',
				templateUrl: 'app/alertlist/views/list.html',
				controller: 'AlertListController as vm',
				title: 'Alertes'
			})
			.state('surveys', {
				url: '/surveys',
				templateUrl: 'app/survey/views/index.html',
				title: 'Sondages'
			})
			.state('surveys.scores', {
				url: '/scores',
				templateUrl: 'app/survey/views/scores.html',
				title: 'Sondage / Scores',
				controller: 'SurveyScoresController as vm'
			})
			.state('surveys.list', {
				url: '/list',
				templateUrl: 'app/survey/views/list.html',
				title: 'Sondage / Liste',
				controller: 'SurveyListController as vm'
			})
			.state('survey-detail', {
				url: '/survey-detail/:surveyID/:mode?',
				templateUrl: 'app/survey/views/detail.html',
				controller: 'SurveyDetailController as vm',
				title: 'Detail de sondage',
				param: {
					surveyID: null,
					mode: 'detail'
				}
			})
			.state('actionplans', {
				url: '/actionplans',
				templateUrl: 'app/actionplans/views/list.html',
				title: 'Plan d\'action',
				controller: 'ActionPlansListController as vm'
			})
		})
}())
